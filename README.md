This repository provides a collection of Python routines to manage 
a hard drive with Kepler light curves calibrated with the KEPSEISMIC
method. In particular, it allows to create and manipulate
an index to quickly look for light curves and PSD given their 
corresponding KIC.
