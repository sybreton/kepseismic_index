from .kic_index import * 
from astropy.io import fits
import numpy as np
import apollinaire as apn
import matplotlib
import matplotlib.pyplot as plt

'''
Plotting functions.
'''

def get_and_plot (kic, Dir='/Volumes/LaCie_ORANGE2/DATA/KEPLER_LC', 
                  index_name='index_55d.dat', figsize=(10,8)) :
    '''
    Get a LC corresponding to a given KIC and plot light curve and
    PSD. 
    '''
    
    index = load_index (path.join (Dir, index_name))
    lcPath = index.getLightCurvePath (kic)
    
    hdu = fits.open (lcPath) [0]
    data = np.array (hdu.data)
    stamp, lc = data[:,0], data[:,1]
    dt = np.median (np.diff (stamp)) * 86400
    freq, psd = apn.psd.series_to_psd (lc, dt=dt, correct_dc=True)
    
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots (2, 2, figsize=figsize)

    fig.suptitle ('KIC {:.0f}'.format (kic))
    
    ax1.plot (stamp - stamp[0], lc, color='black', ls='none',
              marker='.', ms=1)
    ax1.set_ylabel (r'Flux (ppm)')
    ax1.set_xlabel (r'Time (days)')
    
    ax2.plot (freq*1e6, psd*1e-6, color='black')
    ax2.set_ylabel (r'PSD (ppm$^2$/$\mu$Hz)')
    ax2.set_xlabel (r'$\nu$ ($\mu$Hz)')
    ax2.set_yscale ('log')
    ax2.set_xscale ('log')
    
    ax3.plot (freq*1e6, psd*1e-6, color='black')
    ax3.set_ylabel (r'PSD (ppm$^2$/$\mu$Hz)')
    ax3.set_xlabel (r'$\nu$ ($\mu$Hz)')
    ax3.set_xlim (0, 75)
    
    ax4.plot (1/(86400*freq[freq!=0]), psd[freq!=0]*1e-6, color='black')
    ax4.set_ylabel (r'PSD (ppm$^2$/$\mu$Hz)')
    ax4.set_xlabel (r'Period (days)')
    ax4.set_xlim (0.1, 1)
    
    return fig


def get_and_plot_multi (list_kic, Dir='/Volumes/LaCie_ORANGE2/DATA/KEPLER_LC',
                        index_name='index_55d.dat') :
    '''
    Get LC corresponding to a several KIC and plot light curveand
    PSD. 
    '''
    
    index = load_index (path.join (Dir, index_name))
    
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots (2, 2, figsize=(10,8))
    ax1.set_ylabel (r'Flux (ppm)')
    ax1.set_xlabel (r'Time (days)')
    
    ax2.set_ylabel (r'PSD (ppm$^2$/$\mu$Hz)')
    ax2.set_xlabel (r'$\nu$ ($\mu$Hz)')
    ax2.set_yscale ('log')
    ax2.set_xscale ('log')
    
    ax3.set_ylabel (r'PSD (ppm$^2$/$\mu$Hz)')
    ax3.set_xlabel (r'$\nu$ ($\mu$Hz)')
    ax3.set_xlim (0, 75)
    
    ax4.set_ylabel (r'PSD (ppm$^2$/$\mu$Hz)')
    ax4.set_xlabel (r'Period (days)')
    ax4.set_xlim (0.1, 1)
    
    for kic in list_kic :
        lcPath = index.getLightCurvePath (kic)

        hdu = fits.open (lcPath) [0]
        data = np.array (hdu.data)
        stamp, lc = data[:,0], data[:,1]
        dt = np.median (np.diff (stamp)) * 86400
        freq, psd = apn.psd.series_to_psd (lc, dt=dt, correct_dc=True)

        ax1.plot (stamp - stamp[0], lc, ls='none',
                  marker='.', ms=1, label=kic)
        ax2.plot (freq*1e6, psd*1e-6)
        ax3.plot (freq*1e6, psd*1e-6)
        ax4.plot (1/(86400*freq[freq!=0]), psd[freq!=0]*1e-6, label=kic)
    
    ax4.legend ()

    return fig

def fold (stamp, lc, phase) :
  '''
  Fold a light curve as a function of the given phase.

  Returns
  -------
    The folded light curve with folded stamp reordered.
  '''
  stamp_folded = (stamp-stamp[0])%phase
  new_index = np.argsort (stamp_folded)
  stamp_folded = stamp_folded[new_index]
  folded_lc = np.copy (lc) 
  folded_lc = folded_lc[new_index]

  return stamp_folded, folded_lc

def fold_and_plot (stamp, lc, phase, smoothing=None,
                   figsize=(6,6)) :
  '''
  Fold a light curve, smooth if required and plot results.

  Returns
  -------
    The Figure used to plot the folded light curve.
  '''

  stamp_folded, folded_lc = fold (stamp, lc, phase)

  if smoothing is not None :
    smoothed = apn.peakbagging.smooth (folded_lc, smoothing)
    smoothed = smoothed[::smoothing]
    stamp_smooth = stamp_folded[::smoothing]

  fig, ax = plt.subplots (1, 1, figsize=figsize)

  ax.scatter (stamp_folded, folded_lc, color='black', marker='o', s=5, label='Long cadence')
  ax.scatter (stamp_smooth, smoothed, color='darkorange', marker='o', s=10, label='Long cadence (smoothed)')

  ax.set_xlabel ('Phase (day)')
  ax.set_ylabel ('Flux (ppm)')

  ax.set_title ('Phase: {:.2f} day'.format (phase))

  return fig

def get_and_plot_phase_folding (kic, Dir='/Volumes/LaCie_ORANGE2/DATA/KEPLER_LC', 
                                index_name='index_55d.dat', figsize=(10,8), phase=None,
                                smoothing=None) :
    '''
    Get a LC corresponding to a given KIC and plot light curve and
    PSD. 
    '''
    
    index = load_index (path.join (Dir, index_name))
    lcPath = index.getLightCurvePath (kic)
    
    hdu = fits.open (lcPath) [0]
    data = np.array (hdu.data)
    stamp, lc = data[:,0], data[:,1]

    if phase is None :
      # Use the maximum value of PSD above 0.5 muHz to phase.
      dt = np.median (np.diff (stamp)) * 86400
      freq, psd = apn.psd.series_to_psd (lc, dt=dt, correct_dc=True)
      freq_phase = freq[np.argmax (psd[freq>0.5e-6]) + freq[freq<0.5e-6].size]
      phase = 1 / (freq_phase * 86400)
    
    fig = fold_and_plot (stamp, lc, phase, smoothing=smoothing,
                   figsize=figsize)
    
    return fig
