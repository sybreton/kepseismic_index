import os
import glob
import pickle
from os import path

'''
The Index class provides functions to create and manipulate indexes related to
the KEPSEISMIC archive. 
'''

def create_list_files (rootDir, filter_cut=20, all_quarters=True) :
  '''
  Create list of fits file contained in the KEPSEISMIC archive.
  '''

  list_files = glob.glob (path.join (rootDir, '**/*.fits'), recursive=True)
  list_lc = []
  list_psd = []

  signature = str (filter_cut) + '.0000d'
  if all_quarters :
    signature_sc = str (filter_cut) + '.00000d_ppm0_inpaint3_ALLQ' #Signature useful for the SC files
  else :
    signature_sc = str (filter_cut) + '.00000d_ppm0_inpaint3_Start_Q5' #Signature useful for the SC files

  for elt in list_files :
    if 'LC_CORR_FILT_INP' in elt :
      if signature in elt or signature_sc in elt :
        if ('PSD' not in elt) :
          list_lc.append (elt)
        else :
          list_psd.append (elt)

  return list_lc, list_psd

def extract_kic (fitsPath) :
  '''
  Extract kic from a given KIC path
  '''

  basename = path.basename (fitsPath)
  kic = int (basename[4:13])

  return kic

def create_dict (list_files) :
  '''
  Create dictionary from a list of files created by list_files function 
  (with fits files from only one filter). KIC are used as keys for fits file path.
  '''

  kic = []

  for elt in list_files :
    kic.append (extract_kic (elt)) 
  kic_dict = dict (zip (kic, list_files))


  return kic_dict

def load_index (filename) :
  '''
  Load kepseismic index from pickled file.
  '''

  f = open (filename, 'rb') 
  index = pickle.load (f)
  f.close ()

  return index


class KEPSEISMIC_Index :

  def __init__ (self, archiveDir, filter_cut=20, all_quarters=True) :

    '''
    :param path: root directory of the KEPSEISMIC archive.
    :type path: str

    :param filter_cut: filter cut to consider for the data, 20, 55 or 80 days.
      Optional, default 20.
    :type filter_cut: int

    :param all_quarters: if building an index for short cadence lightcurves, set to False to select
      light curves starting at Q5. Optional, default ``False``.
    :type all_quarters: bool
    '''

    if filter_cut not in [2, 20, 55, 80] :
      raise Exception ('filter_cut argument must be 2, 20, 55 or 80')

    list_lc, list_psd = create_list_files (archiveDir, filter_cut=filter_cut, all_quarters=all_quarters)
    dict_lc = create_dict (list_lc)
    dict_psd = create_dict (list_psd)

    self.__filter_cut = filter_cut
    self.__lc = dict_lc
    self.__psd = dict_psd 

  def getIndexFilterCut (self) :
    '''
    Get KEPSEISMIC Index filter cut.
    '''
    return self.__filter_cut

  def getLightCurvePath (self, kic) :
    '''
    Return light curve path for given kic.
    '''
    return self.__lc[kic]

  def getPSDPath (self, kic) :
    '''
    Return PSD path for given kic.
    '''
    return self.__psd[kic]

  def getDict (self, dict_type='lc') :

    if dict_type not in ['lc', 'psd'] :
      raise Exception ('dict_type must be lc or psd')

    if dict_type=='lc' :
      return self.__lc
    if dict_type=='psd' :
      return self.__psd

  def save (self, filename) :
    '''
    Save KEPSEISMIC index object with the pickle module.
    '''
  
    f = open (filename, 'wb')
    pickle.dump (self, f)
    f.close ()
    


